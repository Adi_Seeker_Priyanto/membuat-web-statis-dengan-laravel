<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/casts');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('casts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('casts.edit', compact('casts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/casts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts');
    }
}
