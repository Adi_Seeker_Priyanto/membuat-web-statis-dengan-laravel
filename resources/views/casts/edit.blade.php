@extends('layout.master')

@section('title1')
    15. Laravel CRUD dengan Query Builder
@endsection

@section('title2')
    Daftar Casts
@endsection

@section('content')
  <div>
    <form action="/casts/{{$casts->id}}" method="POST">
      @method('put')
      @csrf
      <div class="form-group">
        <label for="nama">Nama :</label>
        <input type="text" class="form-control" name="nama" value="{{$casts->nama}}" id="nama" placeholder="Masukkan Nama">
        @error('nama')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur :</label>
        <input type="number" class="form-control" name="umur" value="{{$casts->umur}}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Biodata :</label>
        <textarea class="form-control" name="bio" rows="3" id="bio" placeholder="Masukkan Biodata"><?php echo "{{$casts->bio}}" ?></textarea>
        <script>
          document.getElementById('bio').innerHTML="{{$casts->bio}}";
        </script>
        @error('bio')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
@endsection
