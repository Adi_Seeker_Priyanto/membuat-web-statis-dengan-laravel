@extends('layout.master')

@section('title1')
    15. Laravel CRUD dengan Query Builder
@endsection

@section('title2')
    Daftar Casts
@endsection

@section('content')
  <h2>Show Cast {{$casts->id}}</h2>
  <h4>{{$casts->nama}}</h4>
  <p>{{$casts->umur}}</p>
  <p>{{$casts->bio}}</p>
  <br>
  <a href="/casts"><button type="button" class="btn btn-primary">Back</button></a>
@endsection
