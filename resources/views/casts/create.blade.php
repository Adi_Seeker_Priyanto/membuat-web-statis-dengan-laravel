@extends('layout.master')

@section('title1')
    15. Laravel CRUD dengan Query Builder
@endsection

@section('title2')
    Daftar Casts
@endsection

@section('content')
  <div>
      <h2>Tambah Data</h2>
          <form action="/casts" method="POST">
              @csrf
              <div class="form-group">
                  <label for="nama">Nama :</label>
                  <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                  @error('nama')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
              <div class="form-group">
                  <label for="umur">Umur :</label>
                  <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                  @error('umur')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
              <div class="form-group">
                  <label for="bio">Biodata :</label>
                  <textarea class="form-control" name="bio" rows="3" id="bio" placeholder="Masukkan Biodata"></textarea>
                  @error('bio')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
              <button type="submit" class="btn btn-primary">Tambah</button>
          </form>
  </div>
@endsection
