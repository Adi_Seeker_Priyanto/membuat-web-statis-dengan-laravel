@extends('layout.master')

@section('title1')
    13. Membuat Template dengan Blade Laravel
@endsection

@section('title2')
    A. Intro
@endsection

@section('content')
    <h1>SanberBook</h1>
    <br>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan berbagi agar hidup ini semakin santai berkualitas.</p>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowlegde dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="{{ url('/form') }}">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection

@section('footer')
    <b>Quote Today : </b><i>"Ketika kamu tidak memaksa dirimu sendiri untuk bergerak, maka keadaan yang akan memaksamu."</i>
@endsection
