@extends('layout.master')

@section('title1')
    13. Membuat Template dengan Blade Laravel
@endsection

@section('title2')
    C. Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$FirstName}} {{$LastName}}</h1>
    <br>
    <h2>Terimakasih telah bergabung di <a href="{{ url('/') }}">SanberBook</a>. Social Media kita bersama.</h2>
@endsection

@section('footer')
    <b>Quote Today : </b><i>"Dream as if you'll live forever. Live as if you'll die today." [James Dean]</i>
@endsection