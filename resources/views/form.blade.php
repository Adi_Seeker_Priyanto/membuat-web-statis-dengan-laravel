@extends('layout.master')

@section('title1')
    13. Membuat Template dengan Blade Laravel
@endsection

@section('title2')
    B. Form
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <br>
    <h3>Sign Up Form</h3>
    <form action="{{ url('/post') }}" method="POST">
        @csrf
        <fieldset>
            <legend>Biodata:</legend>
            <label>First Name:</label><br>
            <input type="text" name="FirstName" id="firstname"><br><br>
            <label>Last Name:</label><br>
            <input type="text" name="LastName" id="lastname"><br><br>
            <label>Gender:</label><br>
            <input type="radio" name="gender" id="male" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" name="gender" id="female" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" name="gender" id="other" value="Other"> 
            <label for="other">Other</label>
            <br><br>
            <label>Nationality:</label><br>
            <select id="nationality" name="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Australian">Australian</option>
                <option value="French">French</option>        
            </select>
            <br><br> 
            <label>Language Spoken:</label><br>
            <input type="checkbox" id="bahasa indonesia" name="language">Bahasa Indonesia <br>
            <input type="checkbox" id="english" name="language">English <br>
            <input type="checkbox" id="arabic" name="language">Arabic <br>
            <input type="checkbox" id="japanese" name="language">Japanese <br><br>
            <label>Bio:</label><br>
            <textarea id="bio" name="bio" cols="30" rows="10">Isikan biodata Anda secara singkat dan jelas ...</textarea><br><br>
            <input type="submit" id="submit" name="kirim" value="Sign Up">
        </fieldset>
    </form>
@endsection

@section('footer')
    <b>Quote Today : </b><i>"Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. [Jack Ma]"</i>
@endsection