<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/form', 'AuthController@form');

Route::post('/post', 'AuthController@kirim');

Route::get('/data-tables', function(){
    return view('table.datatable');
});

Route::get('/table', function(){
  return view('.table.table');
});

Route::get('/casts', 'CastsController@index');
Route::get('/casts/create', 'CastsController@create');
Route::post('/casts', 'CastsController@store');
Route::get('/casts/{casts_id}', 'CastsController@show');
Route::get('/casts/{casts_id}/edit', 'CastsController@edit');
Route::put('/casts/{casts_id}', 'CastsController@update');
Route::delete('/casts/{casts_id}', 'CastsController@destroy');
