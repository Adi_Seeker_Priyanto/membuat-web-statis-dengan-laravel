<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('films_id');
            $table->unsignedBigInteger('casts_id');
            $table->char('nama', 45);
            $table->timestamps();

            $table->foreign('films_id')->references('id')->on('films');
            $table->foreign('casts_id')->references('id')->on('casts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perans');
    }
}
